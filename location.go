package darkman

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/WhyNotHugo/darkman/geoclue"
)

// NOTE: Geoclue continues polling in the background every few minutes, so if
// we fail to start or stop it, we hard fail and exit. Geoclue will detect that
// we (the client) has existed, and stop polling.
//
// Errors here are hard to handle, since we can't know geoclue's state, and we
// can't control it and tell it to stop either.

// Returns the path to the location cache file.
func locationCachePath() (string, error) {
	if cacheDir, ok := os.LookupEnv("XDG_CACHE_HOME"); ok {
		return filepath.Join(cacheDir, "darkman/location.json"), nil
	} else {
		home, err := os.UserHomeDir()
		if err != nil {
			return "", fmt.Errorf("failed to resolve user home: %v", err)
		}
		return filepath.Join(home, ".cache/darkman/location.json"), nil
	}
}

func saveLocationToCache(loc geoclue.Location) error {
	cacheFilePath, err := locationCachePath()
	if err != nil {
		return fmt.Errorf("error finding location cache: %v", err)
	}

	marshalled, err := json.Marshal(loc)
	if err != nil {
		return fmt.Errorf("error marshalling location: %v", err)
	}

	err = os.MkdirAll(filepath.Dir(cacheFilePath), os.ModePerm)
	if err != nil {
		return fmt.Errorf("error creating parent directories: %v", err)
	}

	err = os.WriteFile(cacheFilePath, marshalled, os.FileMode(0600))
	return fmt.Errorf("error writing location: %v", err)
}

func readLocationFromCache() (*geoclue.Location, error) {
	cacheFilePath, err := locationCachePath()
	if err != nil {
		return nil, fmt.Errorf("error finding location cache: %v", err)
	}

	data, err := os.ReadFile(cacheFilePath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}
		return nil, fmt.Errorf("error reading cached location: %v", err)
	}

	location := &geoclue.Location{}
	if err = json.Unmarshal(data, location); err != nil {
		return nil, fmt.Errorf("error parsing cached location: %v", err)
	}

	return location, nil
}

// Initialise geoclue. Note that we have our own channel where we yield
// locations, and Geoclient has its own. We act as middleman here since we also
// keep the last location cached.
func initGeoclue(ctx context.Context, onLocation chan (geoclue.Location)) (client *geoclue.Geoclient, err error) {
	client, err = geoclue.NewClient(ctx, "darkman", time.Minute, 40000, 3600*4)
	if err != nil {
		return nil, err
	}

	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case loc := <-client.Locations:
				if err := saveLocationToCache(loc); err != nil {
					log.Println("Error saving location to cache: ", loc)
				} else {
					log.Println("Saved location to cache.")
				}

				onLocation <- loc
			}
		}
	}()

	return
}

// Periodically fetch the current location.
//
// By default, we indicate set geoclue in a rather passive mode; it'll ignore
// location changes that occur in less than four hours, or of less than 40km.
func GetLocations(ctx context.Context, onLocation chan (geoclue.Location)) (err error) {
	if _, err = initGeoclue(ctx, onLocation); err != nil {
		return fmt.Errorf("error initialising geoclue: %v", err)
	}
	return nil

}
